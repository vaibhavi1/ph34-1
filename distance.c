#include <stdio.h>
#include <math.h>
int main()
{float x1,x2,y1,y2,z;
printf("Please enter first pair of points\n");
scanf("%f %f",&x1,&y1);
printf("Please enter second pair of points\n");
scanf("%f %f",&x2,&y2);
z=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
printf("Distance between two points is %f",z);
return 0;
}
//Distance using functions
#include <stdio.h>
#include <math.h>
float dist(float a,float b,float c,float d)
{
float result;
result=sqrt(pow(c-a,2)+pow(d-b,2));
return result;
}
int main()
{
float a,b,c,d,e;
printf("Enter 4 numbers \n");
scanf("%f %f %f %f",&a,&b,&c,&d);
e=dist(a,b,c,d);
printf("Distance is %f",e);
return 0;
}
